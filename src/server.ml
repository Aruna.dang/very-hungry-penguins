(** Server main file *)

(** initialisation function *)
let init_server () =

  (* TODO: parse this from args *)
  let port = 10234 in

  Utils._log ("main: starting a server with port = " ^ (string_of_int port));

  Network.server_start port
  (*
  (* TODO: wait for clients :) *)
  let clients = Array.make 1 1 in

  Utils._log "The game will start soon.";

  clients
  *)

let get_clients_msg clients =
  List.map (function
      | Network.ClientMsg (c, x) -> x, c
      | Network.ClientDisconnect c -> Msg.Disconnected, c
    ) (Network.read_clients_requests !clients)

let rec hello (clients, mutex) =
  Utils._log "hello: waiting for new clients";
  let client = Network.wait_client_connection () in
  Utils._log "hello: waiting for the mutex";
  Mutex.lock mutex;
  Utils._log "hello: adding a new client";
  clients := client::!clients;
  Mutex.unlock mutex;
  hello (clients, mutex)

let rec handle_new_clients (clients, mutexClients, games, mutexGames) =

  Mutex.lock mutexClients;
  let msg = get_clients_msg clients in
  Mutex.unlock mutexClients;

  List.iter (fun msg ->
      match msg with
      | Msg.Disconnected, client ->
        Utils._log "handleHello: client disconnected";
        Utils._log "handleHello: waiting for the mutexClients";
        Mutex.lock mutexClients;
        let newClients = List.filter (fun el -> el <> client) !clients in
        Utils._log "handleHello: removing the disconnected client";
        clients := newClients;
        Mutex.unlock mutexClients;
      | Msg.NewGame (gameName, mapFileName, playerTypeList, turn), client -> begin
          Utils._log "handleHello: new game requested";
          Utils._log "handleHello: waiting for the mutexGames";
          Mutex.lock mutexGames;
          Utils._log "handleHello: adding a new game...";
          Hashtbl.add games gameName (gameName, mapFileName, playerTypeList, [client]);
          Mutex.unlock mutexGames;
        end
      | _ -> ();
    ) msg;

  handle_new_clients (clients, mutexClients, games, mutexGames)

let _ =

  Utils._log "main: initialisation of the server";
  init_server ();

  let games = Hashtbl.create 20 in

  Utils._log "main: lauching hello thread";
  let newClients = ref [] in
  let mutexNewClients = Mutex.create () in
  let mutexGames = Mutex.create () in
  let hello = Thread.create hello (newClients, mutexNewClients) in
  (* thread qui accepte les connections *)

  Utils._log "main: launching handleHello thread";
  (* thread qui parle aux nouveaux arrivants *)
  let handleHello = Thread.create handle_new_clients (newClients, mutexNewClients, games, mutexGames) in

  (* TODO: thread qui itère sur games et lance les parties complètes *)

  Utils._log "main: entering the main loop";
  (* quand une partie est complète, nouvelle thread pour cette partie *)


  Thread.join hello;
  Thread.join handleHello;

  ()
