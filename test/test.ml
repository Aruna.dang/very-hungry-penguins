(** Test file with tests, because tests are great *)

(** The tests **)

(** Tests for the module Move*)
open Move
open Paths

let compare_paire x y s1 s2=
	Alcotest.(check int) s1 (fst(x)) (fst(y));
	Alcotest.(check int) s2 (snd(x)) (snd(y))

let rec compare_paire_list x y s =
	match (x,y) with
	([], []) -> ()
	|([], t::q)-> Alcotest.(check int) "même longueur" 0 1
	|(t::q, [])-> Alcotest.(check int) "même longueur" 0 1
	|(t1::q1, t2::q2) -> (compare_paire t1 t2 s s; compare_paire_list q1 q2 s)


let move_test1 () =
  let got = move (2,3) NE in
  let expected = (1,4) in
  compare_paire got expected "same x coord" "same y coord"

let move_test2 () =
  let got = move (2,3) NW in
  let expected = (1,3) in
  compare_paire got expected "same x coord" "same y coord"

let move_n_test1 () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(1) <- PENGUIN;
  let got = move_n g (1,1) (NE,1) in
  let expected = (0,1) in
  compare_paire got expected "same x coord" "same y coord"

let move_n_test2 () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(1) <- PENGUIN;
  let got = move_n g (1,1) (SW, 1) in
  let expected = (2,0) in
  compare_paire got expected "same x coord" "same y coord"

let move_n_test3 () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(0).(0) <- PENGUIN;
  let got = move_n g (0,0) (SE, 2) in
  let expected = (2,1) in
  compare_paire got expected "same x coord" "same y coord"


let move_n_test4 () =
  let g = Array.make_matrix 4 4 (ICE(1)) in
  g.(3).(0) <- PENGUIN;
  let got = move_n g (3,0) (NE, 3) in
  let expected = (0,1) in
  compare_paire got expected "same x coord" "same y coord"

let legal_move_test () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(1) <- PENGUIN;
  let expected = true in
  let (got,_,_) = legal_move_n g (1,1) (NE,1) in
  Alcotest.(check bool) "The possibility of the move has been detected" got expected;
  let (got,_,_) =legal_move_n g (1,1) (E,1) in
  Alcotest.(check bool) "The possibility of the move has been detected" got expected;
  let (got,_,_) = legal_move_n g (1,1) (NW,1) in
  Alcotest.(check bool) "The possibility of the move has been detected" got expected;
  let (got,_,_) = legal_move_n g (1,1) (SW,1) in
  Alcotest.(check bool) "The possibility of the move has been detected" got expected;
  let (got,_,_) = legal_move_n g (1,1) (SE,1) in
  Alcotest.(check bool) "The possibility of the move has been detected" got expected;
  let (got,_,_) = legal_move_n g (1,1) (W,1) in
  Alcotest.(check bool) "The possibility of the move has been detected" got expected;
  g.(1).(2)<- WATER;
  let expected = false in
  let (got,_,_) = legal_move_n g (1,1) (NE,1) in
  Alcotest.(check bool) "This non-valide move has been rejected" got expected;
  g.(1).(2) <- PENGUIN;
  let (got,_,_) = legal_move_n g (1,1) (NE,1) in
  Alcotest.(check bool) "This non-valide move has been rejected" got expected;
  let (got,_,_) = legal_move_n g (1,1) (SW,2) in
  Alcotest.(check bool) "This non-valide move has been rejected" got expected


(*
let path_of_moves_test ()  =
  let g=  Array.make_matrix 6 6 (ICE(1)) in
  let got = Move.path_of_moves g (3,2)
                             [(NE,2);(E,1);(SW,2);(NW,1);(E,3);(NW,2);(W,1)] in
  let expected = [(1,3);(1,4);(3,3);(2,2);(2,5);(0,4);(0,3)] in
  compare_paire_list got expected "Même chemin"*)


(**Tests for the module computer*)

let max_list_pos_test1 () =
  let (g1, g2, g3) = Computer.max_list_pos [((1,2), 2); ((2,3),4); ((4,5),1)] in
  let (e1,e2) = ((2,3),4) in
  let e3 = 1 in
  compare_paire g1 e1 "same x coord" "same y coord";
  Alcotest.(check int) "same value" g2 e2;
  Alcotest.(check int) "same index" g3 e3

let max_list_pos_test2 () =
  let (g1, g2, g3) = Computer.max_list_pos [((1,2), 2); ((2,3),4); ((4,5),5)] in
  let (e1,e2) = ((4,5),5) in
  let e3 = 2 in
  compare_paire g1 e1 "same x coord" "same y coord";
  Alcotest.(check int) "same value" g2 e2;
  Alcotest.(check int) "same index" g3 e3

let max_list_pos_test3 () =
  let (g1, g2, g3) = Computer.max_list_pos [((1,2), 6); ((2,3),4);((4,5),5)] in
  let (e1,e2) = ((1,2),6) in
  let e3 = 0 in
  compare_paire g1 e1 "same x coord" "same y coord";
  Alcotest.(check int) "same value" g2 e2;
  Alcotest.(check int) "same index" g3 e3

let max_list_pos_test4 () =
  let (g1, g2, g3) = Computer.max_list_pos [] in
  let (e1,e2) = ((-1,-1),0) in
  let e3 = -1 in
  compare_paire g1 e1 "same x coord" "same y coord";
  Alcotest.(check int) "same value" g2 e2;
  Alcotest.(check int) "same index" g3 e3


let ai_test () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(1) <- PENGUIN;
  g.(1).(0) <- ICE(2);
  let (g1,(g2,g3)) = Computer.ai g [(1,1)] in
  let (e1,(e2,e3)) = ((1,1), (W,1)) in
  compare_paire g1 e1 "same penguin" "same penguin";
  if (g2=e2) then Alcotest.(check int) "same distance" g3 e3
  else Alcotest.(check int) "same direction" 0 1
  
let ai_test2 () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(0) <- PENGUIN;
  g.(1).(2) <- ICE(2);
  let (g1,(g2,g3)) = Computer.ai g [(1,0)] in
  let (e1,(e2,e3)) = ((1,0), (E,2)) in
  compare_paire g1 e1 "same penguin" "same penguin";
  if (g2=e2) then Alcotest.(check int) "same distance" g3 e3
  else Alcotest.(check int) "same direction" 0 1


let ai2_test () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(1) <- PENGUIN;
  g.(1).(0) <- ICE(2);
  let (g1,(g2,g3)) = Computer.ai2 g [(1,1)] in
  let (e1,(e2,e3)) = ((1,1), (W,1)) in
  compare_paire g1 e1 "same penguin" "same penguin";
  if (g2=e2) then Alcotest.(check int) "same distance" g3 e3
  else Alcotest.(check int) "same direction" 0 1

let ai2_test2 () =
  let g = Array.make_matrix 3 3 (ICE(1)) in
  g.(1).(0) <- PENGUIN;
  g.(1).(2) <- ICE(3);
  g.(0).(0) <- ICE(2);
  let (g1,(g2,g3)) = Computer.ai2 g [(1,0)] in
  let (e1,(e2,e3)) = ((1,0), (NE,1)) in
  compare_paire g1 e1 "same penguin" "same penguin";
  if (g2=e2) then Alcotest.(check int) "same distance" g3 e3
  else Alcotest.(check int) "same direction" 0 1

(** Basic test *)
let basic () =
  let expected = "jaimeleschips" in
  let got = "jaimeleschips" in
  Alcotest.(check string) "same string" got expected

(** Sets of test *)

(** Set of basic tests *)
let set_basic = [
  "Basic", `Quick, basic;
]

(* ***************** Bitset tests ********************** *)

module I:Bitset.FIN = struct
  type t = int
  let max = 1000
  let to_int n = n mod 1000
  let of_int n = n
end

module S = Bitset.Make(I)

let add_rm_member_test _ =
  let s = ref (S.empty) in
  s := S.add !s (I.of_int 5);
  Alcotest.(check bool) "same bool" (S.member  !s (I.of_int 5)) true;
  Alcotest.(check bool) "same bool" (S.member  !s (I.of_int 4)) false;
  s := S.remove !s (I.of_int 5);
  Alcotest.(check bool) "same bool" (S.member  !s (I.of_int 5)) false

let pairs = S.init (fun i -> let n = I.to_int i in n mod 2 = 0)

let cardinal_test _ =
  Alcotest.(check int) "same cardinal" (S.cardinal pairs) 500

let pairs2 = S.init (fun i -> let n = I.to_int i in n mod 2 = 0)
let p = (S.init (fun i -> let n = I.to_int i in n = 10 || n = 0 || n = 33))

let subset_test _ =
  Alcotest.(check bool) "same bool" (S.subset pairs pairs2) true;
  Alcotest.(check bool) "same bool" (S.subset pairs2 pairs) true;
  Alcotest.(check bool) "same bool" (S.subset p pairs) false

let iter_test _ =
  let sum_p = ref 0 in
  S.iter p (fun el -> sum_p := !sum_p + I.to_int el);
  Alcotest.(check int) "same sum" !sum_p 43



let set_bitset = [
  "add, rm, member", `Quick, add_rm_member_test;
  "cardinal", `Quick, cardinal_test;
  "subset", `Quick, subset_test;
  "iter", `Quick, iter_test
]
(* ******************* End Bitset tests ********************** *)

(* ******************* Priority tests ************************ *)

module T = Priority.Make(Int32)

let test_tas _ =
  let tas = T.create 20 0l "hey" in
  ignore (T.insert tas 10l "blub2");
  ignore (T.insert tas 5l "blib");
  let truc = T.insert tas 8l "BLAB" in
  ignore (T.insert tas 7l "blub");
  let truc2 = T.insert tas 5l "blob" in
  ignore (T.insert tas 3l "bleb");
  ignore (T.insert tas 5l "blab");
  Alcotest.(check bool) "same bool" (T.member tas truc) true;
  T.remove tas truc;
  (try
    T.remove tas truc;
    Alcotest.(check bool) "this should not appear" true false
  with
    _ -> ());
  Alcotest.(check bool) "same bool" (T.member tas truc) false ;
  T.decrease_key tas truc 6l;
  T.decrease_key tas truc2 4l;
  Alcotest.(check int32) "same int32" (T.key (T.extract_min tas)) 3l;
  Alcotest.(check string) "same string" (T.value (T.extract_min tas)) "blob";
  Alcotest.(check int32) "same int32" (T.key (T.extract_min tas)) 5l;
  Alcotest.(check int32) "same int32" (T.key (T.extract_min tas)) 5l;
  Alcotest.(check int32) "same int32" (T.key (T.extract_min tas)) 6l;
  Alcotest.(check int32) "same int32" (T.key (T.extract_min tas)) 7l;
  Alcotest.(check int32) "same int32" (T.key (T.extract_min tas)) 10l;
  (try
    ignore (T.extract_min tas);
    Alcotest.(check bool) "this should not appear" true false
  with
    _ -> ())

let set_priority = ["heap", `Quick, test_tas]


(* ******************* End Priority tests ******************** *)

(* ******************* Txt parse tests ********************** *)
(*m for map p for players*)
let parse_map_test _  =
  let (m,_)  = IO.parse_map "test/map_test.txt" in
  Alcotest.(check bool) "same lines" (m.(1) =
    [|PENGUIN;ICE 2;ICE 2;ICE 2;ICE 2;ICE 2;WATER;
      WATER; ICE 2;ICE 2;ICE 2;ICE 2;ICE 2;PENGUIN|]) true

let parse_players_test _ =
  let (_,p) = IO.parse_map "test/map_test.txt" in
  (*let's check b player*)
  Alcotest.(check bool) "same positions"  ([(1,13);(3,0)] = p.(1)) true

let set_io = ["map parsing",`Quick,parse_map_test;
	      "players parsing",`Quick,parse_players_test]

(* ******************* End txt parse tests ********************** *)



	       
(* ******************* Json parse tests ********************** *)

let parse_name_test _  =
  let (n,_,_,_,_)  = Json_parsing.parse_main_json "test/parse_test.json" in
  Alcotest.(check string) "same map name" n "Niveau 1"

let parse_players_test2 _ =
  let (_,p,_,_,_) = Json_parsing.parse_main_json "test/parse_test.json" in
  Alcotest.(check int) "same number of players" (List.length p) 3;
  match p with
  |[] -> failwith "this is really astonishing"
  |p::_ ->  (*let's check beauGosseDu84*)
    Alcotest.(check bool) "same positions"
			  (p#get_penguins_pos = [|(3,13)|]) true;
    Alcotest.(check bool) "same pseudos"
			  (p#get_name = "beauGosseDu84") true

let parse_turn_test _ =
  let (_,_,_,t,_) = Json_parsing.parse_main_json "test/parse_test.json" in
  Alcotest.(check int) "same turn" t 1

let parse_challenge_test _ =
  let (_,_,_,_,c) = Json_parsing.parse_main_json "test/solo1.json" in
  Alcotest.(check bool) "same challenge" (c = [Move.MAX_FISH 20;
					       Move.MIN_TELEPORT 2;
					       Move.MIN_MOVE 10]) true

let set_json = ["name parsing",`Quick,parse_name_test;
		"players parsing (json)",`Quick,parse_players_test2;
		"turn parsing",`Quick,parse_turn_test;
		"challenge parsing",`Quick,parse_challenge_test]

(* ******************* End json parse tests ********************** *)



		 
(* ******************* Paths tests ******************************** *)

let accessible_test _ =
  let gs = new Game_state.game_state (JSON "test/accessible_test.json") in 
  let module Grid : S= struct
    let grid = gs#get_map ()
  end in
  let module Path = Make (Grid) in
  let acc = Path.grid_of_set (Path.accessible gs Path.grid_set (1,5)) in
  Alcotest.(check bool) "same cell 1" (acc.(1).(5) = (ICE 1)) true;
  Alcotest.(check bool) "same cell 2" (acc.(1).(4) = (ICE 1)) true;
  Alcotest.(check bool) "same cell 3" (acc.(1).(3) = (ICE 1)) true;
  Alcotest.(check bool) "same cell 4" (acc.(2).(1) = WATER) true


let split_exc_test _ =
 let gs = new Game_state.game_state (JSON "test/accessible_test.json") in 
  let module Grid : S= struct
    let grid = gs#get_map ()	   
  end in
  let module Path = Make (Grid) in
  let spl_exc  = Path.split_exclusive gs Path.grid_set (1,1) in
  match spl_exc with
  |[s] -> let g = Path.grid_of_set s in
	  Alcotest.(check bool) "same cell 1" (g.(0).(1) = (ICE 1)) true;
	  Alcotest.(check bool) "same cell 2" (g.(2).(1) = WATER) true
  |_ -> failwith "exclusive scoop : split_exclusive is not working"


let solo_test s pos max_fish=
  let gs = new Game_state.game_state (JSON s) in 
  let module Grid : S= struct
    let grid = gs#get_map ()	   
  end in
  let module Path = Make (Grid) in  
  let acc = Path.grid_of_set (Path.accessible gs Path.grid_set pos) in
  let (max_fish_got,p) = Path.max_path gs pos in
  gs#pp_grid Format.std_formatter acc;
  gs#pp_path Format.std_formatter p;
  Alcotest.(check int) "same max_fish" max_fish max_fish_got

let solo1_test _ =
 solo_test "test/solo1.json" (9,8) 20

let solo2_test _ =
  solo_test "test/solo2.json" (0,0) 29

			   
let set_paths = ["accessible",`Quick, accessible_test;
		 "split exclusive", `Quick, split_exc_test;
		 (* "solo1",`Quick, solo1_test;*)
		 "solo2",`Quick, solo2_test]

(* ********************* End path tests ************************ *)


  
let set_move_test = [
  "move test n°1", `Quick, move_test1;
  "move test n°2", `Quick, move_test2;
  "move_n test n°1", `Quick, move_n_test1;
  "move_n test n°2", `Quick, move_n_test2;
  "move_n test n°3", `Quick, move_n_test3;
  "move_n test n°4", `Quick, move_n_test4;
  (*"path_of_moves test", `Quick, path_of_moves_test;*)
]

let set_computer = [
  "max_list_pos test n°1", `Quick, max_list_pos_test1;
  "max_list_pos test n°2", `Quick, max_list_pos_test2;
  "max_list_pos test n°3", `Quick, max_list_pos_test3;
  "max_list_pos test n°4", `Quick, max_list_pos_test4;
  "ai test n°1", `Quick, ai_test;
  "ai test n°2", `Quick, ai_test2;
  "ai2 test n°1", `Quick, ai2_test;
  "ai2 test n°1", `Quick, ai2_test2;
]

(** Main function, run all sets of tests *)
let () =
  Alcotest.run "Tests"
	       [
		 "Basic", set_basic;
		 "Bitset", set_bitset;
		 "Computer", set_computer;
		 "IO (txt parsing)", set_io;
		 "JSON", set_json;
		 "Move", set_move_test;
		 "Paths", set_paths;
		 "Priority", set_priority
	       ]
